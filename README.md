# PwSh.Fw.Iso

| |  linux  |  macos  | windows  |
|---------|---------|---------|----------|
| develop ||![coverage](https://gitlab.com/pwsh.fw/pwsh.fw.iso/badges/develop/coverage.svg?job=linux_pester_job) | ![coverage](https://gitlab.com/pwsh.fw/pwsh.fw.iso/badges/develop/coverage.svg?job=macos_pester_job) | ![coverage](https://gitlab.com/pwsh.fw/pwsh.fw.iso/badges/develop/coverage.svg?job=windows_pester_job) |
| master ||![coverage](https://gitlab.com/pwsh.fw/pwsh.fw.iso/badges/master/coverage.svg?job=linux_pester_job) | ![coverage](https://gitlab.com/pwsh.fw/pwsh.fw.iso/badges/master/coverage.svg?job=macos_pester_job) | ![coverage](https://gitlab.com/pwsh.fw/pwsh.fw.iso/badges/master/coverage.svg?job=windows_pester_job) |

