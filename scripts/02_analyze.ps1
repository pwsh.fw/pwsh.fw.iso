[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force
)

. $PSScriptRoot/00_header.ps1 -SkipNuGet:$SkipNuGet -Force:$Force

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

Install-Module PsScriptAnalyzer
Import-Module PSScriptAnalyzer
Invoke-ScriptAnalyzer -Path "$ROOTDIR/$($Project.Name)" -Recurse
