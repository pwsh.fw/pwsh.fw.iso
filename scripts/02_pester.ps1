[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force
)

. $PSScriptRoot/00_header.ps1 -SkipNuGet:$SkipNuGet -Force:$Force

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

Install-Module Pester -SkipPublisherCheck -MinimumVersion 5.0 -Force
Install-Module PesterMatchHashtable
Import-Module Pester -MinimumVersion 5.0
Get-Module Pester | Format-Table Name, Version, ExportedFunctions
Invoke-Pester $ROOTDIR
$files = Get-ChildItem $ROOTDIR -File -Recurse -Include *.psm1
Invoke-Pester $ROOTDIR -CodeCoverage $files
