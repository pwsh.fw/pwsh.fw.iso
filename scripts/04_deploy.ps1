[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force
)

. $PSScriptRoot/00_header.ps1 -SkipNuGet:$SkipNuGet -Force:$Force

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

Update-ModuleManifest -Path $ROOTDIR/$($project.Name)/$($project.Name).psd1 -ModuleVersion $($project.Version) -PreRelease $($project.PreRelease)
if ($env:CI_COMMIT_BRANCH -eq "master") {
	Publish-Module -Path $ROOTDIR/$($project.Name) -NuGetApiKey $env:NUGET_API_KEY_2020 -Verbose -Force
} else {
	Publish-Module -Name $ROOTDIR/$($project.Name) -NuGetApiKey $env:NUGET_API_KEY_2020 -Verbose -Force -AllowPrerelease
}

Write-Host -ForegroundColor Blue "<< $BASENAME"
