[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force
)

$ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path.TrimEnd('/\')
Write-Output ">> header"

Write-Output "ROOTDIR = $ROOTDIR"

# clean to avoid doubles
Remove-Module PwSh.Fw.*
if (-not ($SkipNuGet)) {
	if (!(Get-PackageProvider -Name NuGet)) { Install-PackageProvider -Name NuGet -Force:$Force -Confirm:$false }
	Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
	Install-Module PwSh.Fw.Core -ErrorAction stop -Force:$Force -AllowPrerelease
	Install-Module PwSh.Fw.BuildHelpers -ErrorAction stop -Force:$Force -AllowPrerelease
}
Import-Module PwSh.Fw.Core -Force:$Force
Import-Module PwSh.Fw.BuildHelpers -Force:$Force -ErrorAction stop

$Global:QUIET = $false
$Global:VERBOSE = $true
$Global:DEBUG = $true
$Global:DEVEL = $true
$Global:TRACE = $true

$PSVersionTable
Get-Module PwSh.Fw.* | Format-Table
$project = Get-Project -Path $ROOTDIR
$project | Format-Table Name, Value
if ($null -eq $project) { Write-Fatal "Project is NULL !!"}

Write-Output "<< header"
