[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force
)

. $PSScriptRoot/00_header.ps1 -SkipNuGet:$SkipNuGet -Force:$Force

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

if (Get-Host) {
	Get-Host
	(Get-Host).ui | ConvertTo-json -depth 4
}
Get-Location
Get-ChildItem Env:\ | Format-Table Name, Value

Write-Host -ForegroundColor Blue "<< $BASENAME"
