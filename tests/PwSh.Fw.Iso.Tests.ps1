﻿BeforeAll {
    if ((Get-Module Pester).Version -lt [version]'5.0.0') { throw "Pester > 5.0.0 is required." }

    $ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path -replace "\$" -replace "/$"
    $BASENAME = Split-Path -Path $PSCommandPath -Leaf
    $ModuleName = $BASENAME -replace ".Tests.ps1"

    # load header
    # . $PSScriptRoot/header.inc.ps1
    $null = Import-Module PwSh.Fw.Core -DisableNameChecking
    $null = Import-Module -FullyQualifiedName $ROOTDIR/$ModuleName/$ModuleName.psm1 -Force -PassThru -ErrorAction stop
}


Describe "PwSh.Fw.Iso" {

    Context "Iso handling" {

        It "Create a new iso file" {
            Remove-Item "$([io.path]::GetTempPath())/${ModuleName}.iso" -ErrorAction SilentlyContinue
            Remove-Item "$([io.path]::GetTempPath())/${ModuleName}-iso" -Recurse -ErrorAction SilentlyContinue
            New-Item "$([io.path]::GetTempPath())/${ModuleName}-iso" -ErrorAction:SilentlyContinue
            New-Item "$([io.path]::GetTempPath())/${ModuleName}-iso/pester.txt" -ErrorAction:SilentlyContinue
            $null = New-IsoFile -Source "$([io.path]::GetTempPath())/${ModuleName}-iso" -Path "$([io.path]::GetTempPath())/${ModuleName}.iso"
            $rc = Test-FileExist -Name "$([io.path]::GetTempPath())/${ModuleName}.iso"
            $rc | Should -Be $true
            # remove-item /pester.txt -Force:$true
        }

        It "Do not overwrite existing iso file" {
            $existing = get-item "$([io.path]::GetTempPath())/${ModuleName}.iso" -ErrorAction SilentlyContinue
            New-Item "$([io.path]::GetTempPath())/${ModuleName}-iso" -ErrorAction:SilentlyContinue
            New-Item "$([io.path]::GetTempPath())/${ModuleName}-iso/pester2.txt" -ErrorAction:SilentlyContinue
            $null = New-IsoFile -Source "$([io.path]::GetTempPath())/${ModuleName}-iso" -Path "$([io.path]::GetTempPath())/${ModuleName}.iso"
            $current = get-item "$([io.path]::GetTempPath())/${ModuleName}.iso" -ErrorAction SilentlyContinue
            ($existing.LastWriteTime -eq $current.LastWriteTime) | Should -Be $true
            # remove-item /pester.txt -Force:$true
        }

        It "Overwrite existing iso file" {
            $existing = get-item "$([io.path]::GetTempPath())/${ModuleName}.iso" -ErrorAction SilentlyContinue
            $rc = Test-FileExist -Name "$([io.path]::GetTempPath())/${ModuleName}.iso"
            $rc | Should -Be $true
            New-Item "$([io.path]::GetTempPath())/${ModuleName}-iso" -ErrorAction:SilentlyContinue
            New-Item "$([io.path]::GetTempPath())/${ModuleName}-iso/pester2.txt" -ErrorAction:SilentlyContinue
            $null = New-IsoFile -Source "$([io.path]::GetTempPath())/${ModuleName}-iso" -Path "$([io.path]::GetTempPath())/${ModuleName}.iso" -Force
            $current = get-item "$([io.path]::GetTempPath())/${ModuleName}.iso" -ErrorAction SilentlyContinue
            ($existing.LastWriteTime -eq $current.LastWriteTime) | Should -Be $false
            # remove-item /pester.txt -Force:$true
        }

        It "Calling without -Passhru returns nothing" {
            Remove-Item "$([io.path]::GetTempPath())/${ModuleName}.iso" -ErrorAction SilentlyContinue
            Remove-Item "$([io.path]::GetTempPath())/${ModuleName}-iso" -Recurse -ErrorAction SilentlyContinue
            New-Item "$([io.path]::GetTempPath())/${ModuleName}-iso" -ErrorAction:SilentlyContinue
            New-Item "$([io.path]::GetTempPath())/${ModuleName}-iso/pester.txt" -ErrorAction:SilentlyContinue
            $iso = New-IsoFile -Source "$([io.path]::GetTempPath())/${ModuleName}-iso" -Path "$([io.path]::GetTempPath())/${ModuleName}.iso"
            $iso | Should -Be $null
        }

        It "Calling with -Passhru returns item" {
            Remove-Item "$([io.path]::GetTempPath())/${ModuleName}.iso" -ErrorAction SilentlyContinue
            Remove-Item "$([io.path]::GetTempPath())/${ModuleName}-iso" -Recurse -ErrorAction SilentlyContinue
            New-Item "$([io.path]::GetTempPath())/${ModuleName}-iso" -ErrorAction:SilentlyContinue
            New-Item "$([io.path]::GetTempPath())/${ModuleName}-iso/pester.txt" -ErrorAction:SilentlyContinue
            $iso = New-IsoFile -Source "$([io.path]::GetTempPath())/${ModuleName}-iso" -Path "$([io.path]::GetTempPath())/${ModuleName}.iso" -PassThru
            $iso | Should -Not -Be $null
        }

    }

}