# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2023-06-29

### Added

- `-PassThru` parameter to all functions

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [1.1.0] - 2023-06-28

### Added

- new code to handle linux platform
- new code to handle macos platform
- `New-IsoFile` is now just a wrapper around `New-IsoFile<<Platform>>`

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [1.0.1] - 2021-04-26

### Changed

- work with powershell 5 Desktop because VMware.Builder does not work with powershell core

## [0.1.0] - 2021-04-14
